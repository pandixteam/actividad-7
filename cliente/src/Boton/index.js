import React, { useState } from "react";
import Modal from 'react-modal';
import './Boton.css';
import axios from 'axios';

const customStyles = {
   overlay: {
       position: 'fixed',
       top: 0,
       left: 0,
       right: 0,
       bottom: 0,
       backgroundColor: 'rgba(255, 255, 255, 0.75)'
   },
   content: {
       top: '50%',
       left: '50%',
       right: 'auto',
       bottom: 'auto',
       marginRight: '-50%',
       background: '#e0bbe4',
       padding: '20px',
       width: '500px',
       overflow:'hidden',
       transform: 'translate(-50%, -50%)',
   },
};

function Boton(props) {

   function onClickButton() {
      alert('Aquí deberá abrirse un modal.')
   }

   let subtitle;
   const [modalIsOpen, setIsOpen] = useState(false);

   function openModal() {
       setIsOpen(true);
   }

   function afterOpenModal() {
       // references are now sync'd and can be accessed.
       subtitle.style.color = '#000';
   }

   function closeModal() {
       setIsOpen(false);
   }
   const [newTareaValue, setNewTareValue] = useState('');
   const [newCategoria, setNewCategoria] = useState('');
   const [prioridad,setPrioridad] = useState('');
   const [date,setDate] = useState('');

   const onChange = (event) => {
      setNewTareValue(event.target.value);
  };

  const notificarme = () =>{
    if (!window.Notification) {
      console.log('Este navegador soporta notificaciones')
      return
    }

    if (Notification.permission === 'granted') {
      new Notification('Tarea agregada correctamente')
    }
    else if(Notification.permission !== 'denied' ||
    Notification.permission === 'default'){
      Notification.requestPermission((permission)=>{
        console.log(permission)
        if (permission === 'granted') {
          new Notification('Tarea agregada correctamente');
        }
      })
    }
  }

  const onSubmit = (event) => {
      event.preventDefault();
      closeModal();
      //props.addTarea(newTareaValue);
      axios.post('api',{
        newTareaValue,
        newCategoria,
        prioridad,
        date
      }).then(res=>
        console.log('Peticion post',res)
        ,setNewTareValue('')
        ,setNewCategoria('Selecciona una categoria')
        ,setPrioridad('Selecciona la prioridad')
        ,setDate('')
        ,notificarme()
        ).catch(err => console.log(err))
  };

   return(
      <section class="contenedor">
      <div class="boton">
         <a href="#" onClick={openModal}>
      <img src="https://cdn-icons-png.flaticon.com/512/37/37770.png" class="burguer"/>   
      </a>
      </div>
      <Modal
                isOpen={modalIsOpen}
                onAfterOpen={afterOpenModal}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Modal"
            >
                <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Nueva tarea</h2>
                <form onSubmit={onSubmit}>
                <input
                    className='input form-control space'
                    value={newTareaValue}
                    onChange={onChange}
                    placeholder="Tarea"
                />
                <label for="selectCategory">Categoria</label><br/>
                <select name="selectCategory" className="space form-select" value={newCategoria} onChange={(event) => setNewCategoria(event.target.value)}>
                    <option>Selecciona una categoria</option>
                    <option value="Personal">Personal</option>
                    <option value="Escolar">Escolar</option>
                </select>
                <label>Prioridad</label><br/>
                <select name="selectPriority" className="space form-select" value={prioridad} onChange={(event)=> setPrioridad(event.target.value)}>
                    <option>Selecciona la prioridad</option>
                    <option value="alta">Alta</option>
                    <option value="baja">Baja</option>
                </select>
                <label>Fecha</label><br/>
                <input type="date" className="space" value={date} onChange={(event)=> setDate(event.target.value)}/>
                    <button className="btn btn-sm btn-light" onClick={closeModal}>Cerrar</button>&nbsp;
                    <button className="btn btn-sm btn-dark" type="submit">Agregar</button>
                </form>
            </Modal>
      </section>
   )
}

export {Boton}