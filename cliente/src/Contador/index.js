import React from "react";
import './Contador.css';
import 'typeface-quicksand';

function Contador({total, completed}) {

  

   return(
    <section>
      <h1>Tareas realizadas: {completed} / {total}</h1>
    </section>
   )
}

export {Contador}