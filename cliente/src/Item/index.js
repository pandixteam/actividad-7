import React, { useEffect, useState } from "react";
import './Item.css';
import axios from "axios";

function Item(props) {

  const deleteNote = async(id) => {
    await axios.delete(`/api/delete-note/${id}`).then(res => {
      console.log(res.data)
    }).catch(err => {
      console.log(err)
    })
  }
  
  const completedNote = async(id) => {
    await axios.put(`/api/completed-note/${id}`).then(res => {
      console.log(res.data)
    }).catch(err => {
      console.log(err)
    })
  }

  function estilo(completed){

    const tachar = {
      'text-decoration': 'line-through'
    };

      if (completed == true) {
        return tachar
      } else{
        return
      }
  }

   return(
    <div class="item">
      <span onClick={() => completedNote(props.note._id)}><box-icon name='check-circle' type='solid' color='#957dad' ></box-icon></span>
      <p class="textito" style={estilo(props.note.Completada)}>{props.note.Tarea}</p>
      <p class="textito" style={estilo(props.note.Completada)}>{props.note.Prioridad}</p>
      <p class="textito" style={estilo(props.note.Completada)}>{props.note.Fecha}</p>
      <span  onClick={() => deleteNote(props.note._id)}><box-icon name='trash-alt' type='solid' color='#957dad' ></box-icon></span>
    </div>
   )
}

export {Item}