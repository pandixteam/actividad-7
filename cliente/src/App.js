import './App.css';
import { Boton } from "./Boton";
import { Buscador } from "./Buscador";
import { Contador } from "./Contador";
import React, { useEffect } from 'react';
import { Categorias } from './Categorias';
import axios from 'axios';
import { set } from 'mongoose';

//const tareasDefault = [
  //{text: "Tarea 1", completed: true},
  //{text: "Tarea 2", completed: true},
  //{text: "Tarea 3", completed: false},
  //{text: "Tarea 4", completed: false},
  //Prueba del key gpg
//] 

function App() {

    
  const [tareas, setTareas] = React.useState([]) 

  useEffect(() => {
    axios.get('/api').then(res => {
      setTareas(res.data)
    }).catch(err => {
      console.log(err)
    })
  }, [])


  const completedTareas = tareas.filter(tarea => tarea.Completada === true).length
  const totalTareas = tareas.length

  const [buscandoValor, setBuscandoValor] = React.useState("");
  let buscarTareas = tareas;

  if (!buscandoValor.length >= 1) {
    buscarTareas = tareas;
  } else {
    buscarTareas = tareas.filter((tarea) => {
      const tareaText = tarea.Tarea.toLowerCase();
      const buscarText = buscandoValor.toLowerCase();

      return tareaText.includes(buscarText);
    });
  }
  

  return (
    <React.Fragment>
      <Contador
        total ={totalTareas}
        completed ={completedTareas}
      />
      <Buscador buscandoValor={buscandoValor} setBuscandoValor={setBuscandoValor}/>
      <Boton/>
      <br/>
      <div className='container '>
      <div class="row justify-content-center">
        <div class="col-2">
          
        </div>
        <div class="col-8">
          
          <Categorias/>
        </div>
        <div class="col-2">
         
        </div>
        </div>
           </div>
    </React.Fragment>
  );
}

export default App;
