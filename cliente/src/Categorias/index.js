import React, {useEffect, useState} from "react";
import axios from "axios";
import './Categorias.css';
import { Tabs, Tab, TabPanel, TabList } from 'react-re-super-tabs'
import CustomTab from './CustomTab'
import { Item } from "../Item";


function Categorias(props) {

  const [notes, setNotes] = useState([])

  useEffect(() => {
    axios.get('/api').then(res => {
      console.log(res.data)
      setNotes(res.data)
    }).catch(err => {
      console.log(err)
    })
  }, [])

    return (
        <Tabs activeTab='about'>
    <TabList>
      <Tab component={CustomTab} label='Personales' id='info' />
      <Tab component={CustomTab} label='Escolares' id='about' />
    </TabList>
    <TabList>
    {
      notes.filter((note) => note.Categoria === 'Personal')
      .map((note) => (
        <TabPanel component={() => 
          <Item note={note} />
        } id='info'/>
      ))
    }
    {
      notes.filter((note) => note.Categoria === 'Escolar')
      .map((note) => (
        <TabPanel component={() => 
          <Item note={note} />
        } id='about'/>
      ))
    }
     
    </TabList>
  </Tabs>
      
    );
   
}

export {Categorias}