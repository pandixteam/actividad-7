import React from "react";
import './Lista.css';

function Lista(props) {
   return(
    <section class="main-lista">
      <ul>
         {props.children}
      </ul>
    </section>
   )
}

export {Lista}