import React from "react";
import './Buscador.css';

function Buscador({buscandoValor, setBuscandoValor}) {

    const Buscar = (event) => {
        setBuscandoValor(event.target.value)
        console.log(buscandoValor)
    }

   return(
      <section class="main-input">
      <div class="main-input-container">
          <span class="searh-icon">
              
          </span>
          <input type="text" placeholder="Buscar nota..." value={buscandoValor} onChange={Buscar}/>
      </div>
      
  </section>
   )
}

export {Buscador}