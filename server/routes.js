// Routes.js - Módulo de rutas
const express = require('express');
const app = express()
const port = 4000;
const cors = require('cors')
const router = express.Router();
//MONGO
const mongojs = require('mongojs');

//MANEJO DE DATOS
/*const db = require('https://data.mongodb-api.com/app/data-gkknk/endpoint/data/v1');
const appTareas = db.collection('tareas');*/
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/AppNotas');
const db = mongoose.connection
const appTareas = db.collection('tareas');

const eschema = mongoose.Schema

const eschemaTarea = new eschema({
    Tarea : String,
    Categoria : String,
    Prioridad : String,
    Fecha : String,
    Completada: Boolean
});

const ModeloTarea = mongoose.model('tareas', eschemaTarea);

db.on('connected', ()=>{console.log('Conexión correcta a mongo')});
db.on('error', ()=>{console.log('Error en la conexión a mongo')});

app.use(cors())
app.use(express.json())

//Importar body parser
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: 'true'}))


router.get('/', (req, res) => {
  ModeloTarea.find({}, function(docs, err){
    if (!err) {
      res.send(docs)
    } else {
      res.send(err)
    }
  });
});

router.post('/', function (req,res) {
  const Tarea = new ModeloTarea ({
    Tarea : req.body.newTareaValue,
    Categoria : req.body.newCategoria,
    Prioridad : req.body.prioridad,
    Fecha : req.body.date,
    Completada: false
  });

    Tarea.save(function (err) {
      if (!err) {
        res.send('Tarea agregada correctamente');
        console.log('Tarea agregada');
      } else {
        res.send(err)
        console.log(err);
      }
    });


  /*res.json({
    ok:true,
    Tarea
  });*/

});

router.delete(`/delete-note/:id`, (req, res) => {
  const id = req.params.id
  console.log(id)
  ModeloTarea.findByIdAndDelete({_id: id},
  (err) => {
    if (!err) {
      res.send("Tarea eliminada correctamente")
      console.log('Tarea eliminada')
    } else {
      res.send(err)
    }
  });
});

router.put(`/completed-note/:id`, (req, res) => {
  const id = req.params.id
  ModeloTarea.findByIdAndUpdate(
    {_id: id},
    {Completada: true},
    (docs, err) => {
      if (!err) {
        res.send(docs)
        console.log('Tarea completada')
      } else {
        res.send(err)
      }
    });
});

app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`)
})

module.exports = router;